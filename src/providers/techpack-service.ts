import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import {Headers} from '@angular/http';


/*
  Generated class for the TechpackService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class TechpackService {

  constructor(public http: Http) {
    console.log('Hello TechpackService Provider');
  }

  public getTeckPackPDF (Auth:string, url:string, query:string) {

        const headers: Headers = new Headers();
        headers.append('Authorization', 'Basic ' + Auth);
        headers.append('Content-Type', 'application/json');
        return (this.http.get('http://' + url +'/Windchill/servlet/rest/rfa/documents/'+ query +'/techpack/pdf',
        {headers: headers}).
        map((response: Response) =>  response));
      }


  //http://windows2008r2.plmtestlab.com:80/Windchill/servlet/rest/rfa/specs/+encodeURI(criteria)+/techpack/pdf

}
