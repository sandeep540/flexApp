import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import {Headers} from '@angular/http';
import { Storage } from '@ionic/storage';
//import {Observable} from 'rxjs/Observable';
/*
  Generated class for the QueryTechPack provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class QueryTechPack {

  constructor(public http: Http, public storage: Storage) {
    console.log('Hello QueryTechPack Provider');
  }

  public getTechPackList(Auth:string, url: string) {
        //console.log('getProductList ----');
        const headers: Headers = new Headers();
        headers.append('Authorization', 'Basic ' + Auth);
        headers.append('Content-Type', 'application/json');
        return (this.http.get('http://' + url +'/Windchill/servlet/rest/rfa/instances?module=DOCUMENT&type=com.lcs.wc.document.LCSDocument|Generated Tech Pack',
        {headers: headers}).
        map((response: Response) =>  response.json()));
      }

}
