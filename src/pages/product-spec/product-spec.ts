import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import {  Response } from '@angular/http';
import { QueryTechPack } from '../../providers/query-tech-pack';
import { Storage } from '@ionic/storage';

import { AuthService } from '../../providers/auth-service';
import { TechpackService } from '../../providers/techpack-service';
/**
 * Generated class for the ProductSpec page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-product-spec',
  templateUrl: 'product-spec.html',
  providers: [QueryTechPack, TechpackService]
})
export class ProductSpec {
  tpacks:any[];
  Auth;
  url;

  constructor(public navCtrl: NavController, private auth: AuthService, private tpservice: QueryTechPack,
    private storage: Storage, private techpackPDF: TechpackService) {


    this.init().then((values) => {tpservice.getTechPackList(this.Auth, this.url)
      .subscribe(
        resBody => this.tpacks = resBody.elements,
        error => console.error('Error: ' + error)),
        () => console.log('Completed retriving the Seasons!' ) });
  }

  // Form the URL
  public downloadTechPack (tpack: String) {

    let vrId = tpack['LCSDOCUMENT.BRANCHIDITERATIONINFO'];
    vrId = 'VR:com.lcs.wc.document.LCSDocument:' + vrId;

    this.techpackPDF.getTeckPackPDF( this.Auth, this.url, vrId).subscribe(data => this.downloadFile(data)),//console.log(data),
                 error => console.log("Error downloading the file."),
                 () => console.info("OK");


  }
  // Hit the URL to get PDF
  public downloadFile (data: Response) {
  //var blob = new Blob([data], { type: 'application/json' });
  //var url= window.URL.createObjectURL(blob);
  window.open(data.url);
}

public init() {

        //  private getAuthHeaders() { return Observable.fromPromise(this.storage.get('Auth')); }
        let promiseList: Promise<any>[] = [];
        promiseList.push(
         this.storage.get('Auth').then((Auth) => {
              console.log('Retrived Auth is', Auth);
              this.Auth = Auth;

            } ));
        promiseList.push(
            this.storage.get('url').then((url) => {
              console.log('Retrived url is', url);
              this.url = url;
            } ));

        return Promise.all(promiseList);
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductSpec');
  }

}
