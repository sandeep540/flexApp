import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductSpec } from './product-spec';

@NgModule({
  declarations: [
    ProductSpec,
  ],
  imports: [
    IonicPageModule.forChild(ProductSpec),
  ],
  exports: [
    ProductSpec
  ]
})
export class ProductSpecModule {}
