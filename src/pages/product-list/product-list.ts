import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProductDetail } from '../product-detail/product-detail';
/**
 * Generated class for the ProductList page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-product-list',
  templateUrl: 'product-list.html',
})
export class ProductList {

products: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.products = navParams.get("products");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductList');
  }

  showProductDetails (product: String) {
      // alert ('Success');
      this.navCtrl.push(ProductDetail, {product: product})
  }

}
