import { Component } from '@angular/core';

//import { AboutPage } from '../about/about';
// import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { Settings } from '../settings/settings';
import { ProductSpec } from '../product-spec/product-spec';
import { SearchProduct } from '../search-product/search-product';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = ProductSpec;
//  tab3Root = ContactPage;
  tab3Root = Settings;
  tab4Root = SearchProduct;

  constructor() {

  }
}
