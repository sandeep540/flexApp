import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchProduct } from './search-product';

@NgModule({
  declarations: [
    SearchProduct,
  ],
  imports: [
    IonicPageModule.forChild(SearchProduct),
  ],
  exports: [
    SearchProduct
  ]
})
export class SearchProductModule {}
