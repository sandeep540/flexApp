import { Component  } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ProductService } from '../../providers/product-service';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';


import { ProductList } from '../product-list/product-list';
/**
 * Generated class for the SearchProduct page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-search-product',
  templateUrl: 'search-product.html',
  providers: [ProductService]
})
export class SearchProduct {
    searchQuery: string = '';
    items:any[];

    Auth;
    url;

  constructor(public navCtrl: NavController, public navParams: NavParams, private productservice: ProductService, private storage: Storage) {
    this.initializeItems();
  }

  initializeItems() {
  }

  public searchProduct() {
  //  alert('Test');
    this.init().then((values) => {  this.productservice.getProductList(this.searchQuery, this.Auth, this.url)
        .subscribe(
          resBody => this.items = resBody.elements,
          error => console.error('Error: ' + error),
          () => {console.log('completed subscription', this.items)
                this.navCtrl.push(ProductList, {products: this.items});
        }),
          () => {
            console.log('Issue in retriving the Products!');
          } })
        }


  ionViewDidLoad() {
    //console.log('ionViewDidLoad SearchProduct');
  }

  public init() {
      //  private getAuthHeaders() { return Observable.fromPromise(this.storage.get('Auth')); }
      // let obserList: Observable<any>[] = [];
      // obserList.push(Observable.fromPromise(this.storage.get('Auth')));
      // obserList.push(Observable.fromPromise(this.storage.get('url')));
      //
      // Observable.fromPromise(this.storage.get('Auth')).subscribe(
      //   Auth => this.Auth = Auth,
      //   error => console.error('Error: ' + error),
      //   () => console.log('completed subscription', this.Auth)
      // );
      //
      // Observable.fromPromise(this.storage.get('url')).subscribe(
      //   url => this.url = url,
      //   error => console.error('Error: ' + error),
      //   () => console.log('completed subscription', this.url)
      // );
      //
      //
      // Observable.fromPromise(this.storage.get('Auth'))


      let promiseList: Promise<any>[] = [];
      promiseList.push(
       this.storage.get('Auth').then((Auth) => {
            console.log('Retrived Auth is', Auth);
            this.Auth = Auth;
          } ));
      promiseList.push(
          this.storage.get('url').then((url) => {
            console.log('Retrived url is', url);
            this.url = url;
          } ));

      return Promise.all(promiseList);
}

}
