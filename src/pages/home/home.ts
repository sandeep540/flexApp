import { OnInit} from '@angular/core';
import { NavController } from 'ionic-angular';
import { Component, ViewChild } from '@angular/core';
import { Nav } from 'ionic-angular';
import { Seasons } from '../seasons/seasons';
import { SeasonService } from '../../providers/season-service';
import { Storage } from '@ionic/storage';

import { AuthService } from '../../providers/auth-service';

import { ScanQR } from '../scan-qr/scan-qr';
import { ContactPage } from '../contact/contact';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [SeasonService]
})
export class HomePage  implements OnInit {
  seasons:any[];
  Auth;
  url;
  @ViewChild(Nav) nav: Nav;
  pages: Array<{title: string, component: any}>;

  constructor(public navCtrl: NavController, private auth: AuthService, private seasonservice: SeasonService, private storage: Storage) {


    this.init().then((values) => {seasonservice.getSeasonList(this.Auth, this.url)
      .subscribe(
        resBody => this.seasons = resBody.elements,
        error => console.error('Error: ' + error)),
        () => console.log('Completed retriving the Seasons!' ) });

        // used for an example of ngFor and navigation
            this.pages = [
              { title: 'Scan-QR', component: ScanQR },
              { title: 'Contact', component: ContactPage }
            ];
  }


    openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }


  showSeasonDetails (season: String) {
      // alert ('Success');
      this.navCtrl.push(Seasons, {season: season})
  }

// One Click of logout, clear the storage, clear current user, go to Home.
  logout() {
    this.storage.clear();
    this.auth.logout();
  //  this.navCtrl.setRoot(AppModule);
  }

  public init() {

          //  private getAuthHeaders() { return Observable.fromPromise(this.storage.get('Auth')); }
          let promiseList: Promise<any>[] = [];
          promiseList.push(
           this.storage.get('Auth').then((Auth) => {
                console.log('Retrived Auth is', Auth);
                this.Auth = Auth;

              } ));
          promiseList.push(
              this.storage.get('url').then((url) => {
                console.log('Retrived url is', url);
                this.url = url;
              } ));

          return Promise.all(promiseList);
}

ngOnInit() { }

}
