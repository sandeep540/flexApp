import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Seasons } from './seasons';

@NgModule({
  declarations: [
    Seasons,
  ],
  imports: [
    IonicPageModule.forChild(Seasons),
  ],
  exports: [
    Seasons
  ]
})
export class SeasonsModule {}
