import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ProductDetailsService } from '../../providers/product-details-service';
/**
 * Generated class for the ProductDetail page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-product-detail',
  templateUrl: 'product-detail.html',
})
export class ProductDetail {

  product: any;
  Auth;
  url;

  productDetails = {
  'Name': 'Black Footwear',
  'Brand': 'Big Bazar',
  'Season': 'Spring',
}

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage, private sdetails: ProductDetailsService) {
    this.product = navParams.get('product');
    //let url = storage.get('url');
    //console.log(url);

    let vrId = this.product['LCSPRODUCT.BRANCHIDITERATIONINFO'];
    vrId = 'VR:com.lcs.wc.product.LCSProduct:' + vrId;

    this.init().then((values) => {  this.sdetails.getProductDetails(vrId, this.Auth, this.url)
        .subscribe(
          resBody => this.productDetails = resBody.attributes,
          error => console.error('Error: ' + error)),
          () => console.log('Completed retriving the Product Details!') });


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductDetail');
  }

  public init() {
          //  private getAuthHeaders() { return Observable.fromPromise(this.storage.get('Auth')); }
          let promiseList: Promise<any>[] = [];
          promiseList.push(
           this.storage.get('Auth').then((Auth) => {
                console.log('Retrived Auth is', Auth);
                this.Auth = Auth;

              } ));
          promiseList.push(
              this.storage.get('url').then((url) => {
                console.log('Retrived url is', url);
                this.url = url;
              } ));

          return Promise.all(promiseList);
}

}
